require essioc
require vac_ctrl_mks946_937b

iocshLoad("${essioc_DIR}/common_config.iocsh")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-dtl-30-u006.tn.esss.lu.se")

epicsEnvSet("DEVICE_NAME", "Spk-010Crm:Vac-VEG-01100")
epicsEnvSet("MOXA_PORT", "4001")
epicsEnvSet("SN_A", "1906280610")
epicsEnvSet("SN_B", "1908070729")
epicsEnvSet("SN_C", "1908061451")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = $(DEVICE_NAME), BOARD_A_SERIAL_NUMBER = $(SN_A=9999999909), BOARD_B_SERIAL_NUMBER = $(SN_B=9999999909), BOARD_C_SERIAL_NUMBER = $(SN_C=9999999909), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGP-01100, CHANNEL = A1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: Isolation Vacuum OK'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: VVA-02100 Low vac lim Itlck'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGC-01100, CHANNEL = B1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC: VVA-02100 High vac lim Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: VVA-02100 Auto close'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-010Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGP-01100, CHANNEL = A2, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: Isolation Vacuum OK'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: VVA-02100 Low vac lim Itlck'")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGC-01100, CHANNEL = C1, CONTROLLERNAME = $(DEVICE_NAME)")

iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC: VVA-02100 High vac lim Itlck'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: VVA-02100 Auto close'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-020Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")

